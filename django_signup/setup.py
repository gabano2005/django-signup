from setuptools import setup


setup(
    name="django-signup",
    version='0.0.2',
    packages=['signup'],
    package_data={'signup':[
        'templates/*',
        'templates/**/*',
        'templates/**/**/*'
    ]},
    include_package_data=True,
    install_requires=[
        'django>=2.0.5,<3',
        'cryptography==2.2'
    ]
)
