from django import forms
from django.core.serializers.json import json

class SignupPersonInfoForm(forms.Form):
    first_name = forms.CharField(max_length=25)
    last_name =forms.CharField(max_length=25)
    email =forms.EmailField(max_length=50)
    password = forms.CharField(widget=forms.PasswordInput)
    password_confirm = forms.CharField(widget=forms.PasswordInput)
    phone = forms.CharField(max_length=15,required=False)

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password","")
        password_confirm = cleaned_data.get("password_confirm","")
        if not password or not password==password_confirm:
            self.add_error("password_confirm","Passwords do not match")

    def to_json(self):
        json_data = json.JSONEncoder().encode(o=self.data)
        return json_data