from cryptography.fernet import Fernet
from django.conf import settings
__secret_key__= getattr(settings,'SECRET_KEY')

def encrypt(data:str):
    return str(Fernet(__secret_key__).encrypt(bytes(data,encoding='utf-8')), encoding='utf-8')

def decrypt(data:str):
    return str(Fernet(__secret_key__).decrypt(bytes(data,'utf-8')), encoding='utf-8')