from django.db import models
from datetime import datetime, timedelta
import base64
import uuid

class SignupTokenManager(models.Manager):
    def find_by_token(self,token):
        return self.all().get(token=token,claimed_at=None,expires_at__gte=datetime.now())

class SignupToken(models.Model):
    created_at = models.DateField(auto_now=True)
    expires_at = models.DateField()
    token = models.UUIDField(db_index=True)
    data = models.CharField(max_length=1024)
    claimed_at = models.DateField(null=True)
    objects=SignupTokenManager()

    def claim(self):
        self.claimed_at = datetime.now()
        self.save()
        
    @staticmethod
    def create_new(data):
        token_expires_at = datetime.now() + timedelta(hours=12)
        token = SignupToken(
            expires_at= token_expires_at,
            token = uuid.uuid4(),
            data = data
        )
        
        token.save()
        return token
