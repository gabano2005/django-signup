from django.urls import path
from .views import SignupPersonInfoView, SignupDoneView, verify_email_view, verify_email_done, verify_email_fail
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('' , SignupPersonInfoView.as_view(), name="signup-form"),
    path('signup/' , SignupPersonInfoView.as_view(), name="signup-form"),
    path('signup-done/', SignupDoneView.as_view(), name="signup-done"),
    path('verify/<token>/', verify_email_view, name="signup-verify-email"),
    path('verify-success/', verify_email_done, name="signup-verify-success"),
    path('verify-fail/', verify_email_fail, name="signup-verify-failure"),
    path('login/', auth_views.LoginView.as_view(template_name="account/login.html"), name="login"),
    path('logout/', auth_views.LogoutView.as_view(), name="logout"),
    path('forgot-password/', auth_views.PasswordResetView.as_view(template_name="account/forgot_password.html"), name="forgot-password"),
    path('forgot-password-done/', auth_views.PasswordResetDoneView.as_view(template_name="account/forgot_password_done.html"), name="password_reset_done"),
    path('confirm/password-reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name="account/password_reset.html"), name="password_reset_confirm"),
    path('confirm/password-reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name="account/password_reset_done.html"), name="password_reset_complete"),
]

#trgmt@riafinancial.com