from django.core.serializers.json import json
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView, View
from django.contrib.auth.models import User
from django.db import models
from .forms import SignupPersonInfoForm
from .models import SignupToken
from .messaging import send_verification_email
from .cryto import encrypt, decrypt

VERIFY_FAIL_REASONS={
    "bad_token":"The link you clicked is not valid",
    "expired": "The link you clicked has expired",
    "error": "There was a problem verifying your email"
}
class SignupPersonInfoView(FormView):
    form_class = SignupPersonInfoForm
    template_name = "signup/profile.html"
    success_url = reverse_lazy("signup-done")

    def form_valid(self,form):
        token=SignupToken.create_new(encrypt(form.to_json()))
        send_verification_email(
            to=form.cleaned_data.get('email'),
            token=token.token,
            request=self.request
        )
        return super().form_valid(form)


class SignupDoneView(TemplateView):
    template_name = "signup/signup-done.html"


def verify_email_view(request,token, *args, **kwargs):
    try:
        signup_token = SignupToken.objects.find_by_token(token)
        if signup_token:
            json_data = json.JSONDecoder().decode(decrypt(signup_token.data))
            user = User(username=json_data.get('email'),
                first_name=json_data.get("first_name"),
                last_name=json_data.get("last_name"),
                email=json_data.get("email")
            )

            user.set_password(json_data.get("password"))
            user.save()

            signup_token.claim()

            #refirect to success
            return redirect("signup-verify-success")
        else:
             return redirect("signup-verify-failure")
    except:
        return redirect("signup-verify-failure")



def verify_email_done(request, * args, **kwargs):
    return render(request,"signup/email-verify-done.html")


def verify_email_fail(request, * args, **kwargs):
    reason_id = request.query_params.get('reason')
    reason = VERIFY_FAIL_REASONS.get(reason_id,'')
    return render(request,"signup/email-verify-fail.html", context={
        "reason": reason
    })


