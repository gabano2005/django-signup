from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings

def send_verification_email(to, token, request=None):
    template_name = "signup/emails/verify-email.html"
    base_url = get_base_url(request)
    message = render_to_string(template_name,context={'token':token, "base_url":base_url}, request=request)
    from_email = getattr(settings,'DEFAULT_FROM_EMAIL')
    send_mail("Confirm email", message,from_email,[to])

def get_base_url(request):
    if(request):
        return "{0}://{1}".format(request.scheme,request.get_host())